# Application #

Eine JAVA Bibliothek zur Vereinfachung der Erstellung von SWING basierten Anwendungen. Viele Funktionen, welche in allen GUI Anwendungen verwendet werden sind in dieser Bibliothek bereits vorbereitet und implementiert.

### Warum diese Bibliothek? ###

Ich habe mit der Entwicklung bereits während meines Studiums begonnen, da bei Studienarbeiten immer wieder viel Zeit damit vergeudet wurde einige grundlegende Features von grafischen Anwendungen neu zu entwickeln.

Sicherlich kann man Code auch von einem Projekt zum nächsten weitergeben und kopieren. Allerdings ergibt sich hierdurch kein wirklicher Fortschritt und von neuen Ideen können ältere Entwicklungen nicht mehr profitieren.

Mit dem Beginn der Entwicklung zu dieser Bibliothek konnten viele der Basisfuntionen implementiert und weiter verbessert werden.

Ich würde mich freuen, wenn ich mit dieser Bibliothek auch anderen helfen kann und freue mich, wenn diese Bibliothek weiter entwickelt wird.

### Was ist in der Bibliothek? ###

Die Bibliothek bietet in der aktuellen Version folgende Funktionen:

* Umschaltung des Look-and-Feel auf alle im System verfügbaren Styles
* Splashscreen der einfach individuell angepasst werden kann
* Integration einer Online-Hilfe mittels Java-Help

### Verwendung ###



### Kontakt? ###

Eigene Ideen oder Bugs gefunden? Bitte meldet euch, ich freue mich über eure Mitarbeit.

* alex@thiel-online.net
