/*
 * Application.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */
 
package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.Frame;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.help.HelpSet;
import javax.help.JHelpContentViewer;

import de.alx_development.Application.languages.Translator;

/**
 * This class is used as main class for displaying
 * the splash screen and info screen and loading another
 * class using the main method of this class.
 * The class to load must be specified by command line
 * parameters.
 * 
 * The first parameter is the name of the class which
 * main method should be loaded. All other parameters
 * are passed as arguments to the class which is loaded.
 * 
 * If you're interested in using the properties capability of the
 * Application, place a properties styled file named like the
 * package of your application concat <code>.properties</code> in
 * the same directory as the application. A file with the same name
 * placed in the user's home directory overwrites the attributes.
 * Using both, you're able to provide system wide settings and
 * user dependent settings at the same time.
 * 
 * @author Alexander Thiel
 */

public class Application
{
	private static HelpSet helpset;
	private static Properties properties = new Properties();
	private static String propertiesFileName = Application.class.getPackage().getName();

	/**
	 * Initializing the logging mechanism
	 */
	public static Logger logger = Logger.getLogger(Application.class.getName());
		
	/**
	 * Main method for this loader. The given arguments
	 * are used to specify the class to load with this loader.
	 * All other arguments are passed to the class which is loaded by
	 * this class.
	 * 
	 * @param args command line arguments
	 * @deprecated Use the new Bootstrapper to load a JFrame with splash screen
	 */
	@Deprecated
	public static void main(String[] args)
	{		 
		/*
		 * Extracting the program to load from the arguments array.
		 * Starting the SplashScreen
		 */
		Frame splashScreen = null;
		Class<?> appclass = null;
		
		/*
		 * Trying to find the requested class
		 */
		String applicationClassName = args[0];
		try
		{
			appclass = Class.forName(applicationClassName);
		}
		catch (ClassNotFoundException e)
		{
			logger.log(Level.SEVERE, "Unable to locate applictaion class "+applicationClassName+" (ClassNotFoundException)");
		}
		
		// Removing the class name from the arguments array
		String newArgs[] = new String[args.length - 1];
		System.arraycopy(args, 1, newArgs, 0, args.length - 1);
		
		/*
		 * Trying to load the properties based on the package name
		 * of the application
		 */
		setPropertiesFileName(appclass.getName());
		loadProperties();
		
		if(new Boolean(Application.getSettingsProperty(applicationClassName+".graphicalInterface", "true")).booleanValue())
		{
			String s = MessageFormat.format(Translator.getInstance().getLocalizedString("LOADING"), new Object[]{applicationClassName});
			splashScreen = SplashScreen.splash(s);
		}		
		
		//Launching the application with the given arguments.
		try
		{
			appclass
			.getMethod("main", new Class[] {String[].class})
			.invoke(null, new Object[] {newArgs});
		}
		catch(Throwable e)
		{
			logger.log(Level.SEVERE, "Failed to launch application: "+e.getLocalizedMessage());
			System.exit(10);
		}
		
		//Destroy SplashScreen
		if(splashScreen != null)
		{
			splashScreen.dispose();
		}
	}
	
	/**
	 * This method is used to load the system properties. If
	 * you'd like to reload the properties just invoke this
	 * method again.
	 */
	public static void loadProperties()
	{
		/*
		 * Loading the system properties
		 */
		Properties systemProperties = new Properties(properties);
		try
		{
			File settingsfile = new File(System.getProperty("user.dir").concat(File.separator)+getPropertiesFileName().concat(".properties"));
			logger.info("Loading properties from systemfile: [" + settingsfile.getAbsolutePath() + "]");
			systemProperties.load(new FileInputStream(settingsfile));
			logger.log(Level.CONFIG, "System properties loaded.");
		}
		catch(Exception exc)
		{
			logger.log(Level.WARNING, "Unable to load system properties: "+exc.getLocalizedMessage());
		}
		
		/*
		 * Loading the user properties
		 */
		Properties userProperties = new Properties(systemProperties);
		try
		{
			File settingsfile = new File(System.getProperty("user.home")+File.separator+".alx"+File.separator+getPropertiesFileName().concat(".properties"));
			logger.info("Loading properties from userfile: [" + settingsfile.getAbsolutePath() + "]");
			userProperties.load(new FileInputStream(settingsfile));
			logger.log(Level.CONFIG, "User properties loaded.");
		}
		catch(Exception exc)
		{
			logger.log(Level.WARNING, "Unable to load user properties: "+exc.getLocalizedMessage());
		}
		properties = userProperties;
	}

	/**
	 * Invoke this method to store the actual properties in the user
	 * directory. System settings are not overwritten.
	 */
	public static void writeProperties()
	{
		try
		{
			File path = new File(System.getProperty("user.home").concat(File.separator).concat(".alx"));
			if(!path.exists()) path.mkdirs();
			File settingsfile = new File(path.getAbsolutePath().concat(File.separator)+getPropertiesFileName().concat(".properties"));
			if( !properties.isEmpty() )
				properties.store(new FileOutputStream(settingsfile), getPropertiesFileName());
		}
		catch (NullPointerException e)
		{
			logger.log(Level.INFO, "No application properties defined, write aborted.");
		}
		catch (FileNotFoundException e)
		{
			logger.log(Level.SEVERE, "FileNotFoundException: Failed to write application properties: "+e.getLocalizedMessage());
		}
		catch (IOException e)
		{
			logger.log(Level.SEVERE, "IOException: Failed to write application properties: "+e.getLocalizedMessage());
		}
	}
	
	/**
	 * This method may be used to access local settings for programs
	 * in this archive, launched by the Application.
	 * 
	 * @param key The requested key
	 * @param defaultValue A default value to return, if no entry can be found
	 * 
	 * @return Either the default value or the settings value.
	 */
	public static String getSettingsProperty(String key, String defaultValue)
	{
		try
		{
			if(properties.getProperty(key) != null)
				defaultValue = properties.getProperty(key);
		}
		catch(Exception exc) { }
		return defaultValue;
	}
	
	/**
	 * This function returns the class specific property value
	 * for the given property key.
	 * 
	 * @param cls Class reference of the invoking class
	 * @param key The values key
	 * @param defaultValue The default return value if no value was found
	 * @return Returning the value stored under the key for this class
	 */
	@SuppressWarnings("rawtypes")
	public static String getSettingsProperty(Class cls, String key, String defaultValue)
	{
		return getSettingsProperty(cls.getName()+"."+key, defaultValue);
	}
	
	/**
	 * This method can be used to store settings in memory.
	 * But be careful, all dynamically set properties are lost
	 * after program exists.
	 * 
	 * @param key
	 * @param value
	 */
	public static void setSettingsProperty(String key, String value)
	{
		try
		{
			if(value == null)
				properties.remove(key);
			else
				properties.setProperty(key, value);
		}
		catch(Exception exc) { }
	}
	
	/**
	 * This method should be used to set a class specific property
	 * in the system properties collection.
	 * 
	 * @param cls Class reference of the invoking class
	 * @param key The values key
	 * @param value The value to store
	 */
	@SuppressWarnings("rawtypes")
	public static void setSettingsProperty(Class cls, String key, String value)
	{
		setSettingsProperty(cls.getName()+"."+key, value);
	}
	
	/**
	 * This method can be used to get access to the translation database
	 * for all programs in this archive.
	 * 
	 * @param key The key for the translation
	 * @return The localized string for the requested key
	 * 
	 * @deprecated Don't use this function anymore. Use the specialized
	 * 	<code>de.alx_development.Application.languages.Translator</code> object instead.
	 * 
	 * @see de.alx_development.Application.languages.Translator
	 */
	@Deprecated
	public static String getLocalizedString(String key)
	{
		return Translator.getInstance().getLocalizedString(key);
	}
	
	/**
	 * Here you can get a username and password for the specified account.
	 * If no data is currently stored for the specified user, a dialog
	 * will automatically appear and ask for the name and password.
	 * 
	 * @param account - The accounts name
	 * 
	 * @return a string array with user as [0] and password as [1]
	 */
	public static String[] getUserAccount(String account)
	{
		String user = getSettingsProperty(account.concat(".user"), System.getProperty("user.name"));
		String password = getSettingsProperty(account.concat(".password"), "#!NO_PASSWORD_AVAILABLE!#");
		
		if(password.equals("#!NO_PASSWORD_AVAILABLE!#"))
		{
			PasswordDialog p = new PasswordDialog(null, MessageFormat.format(Translator.getInstance().getLocalizedString("PASSWORDDIALOG"), new Object[]{account}));
			p.setName(user);
			
			if(p.showDialog())
			{
				user = p.getName();
				password = p.getPassword();
				setSettingsProperty(account.concat(".user"), user);
				setSettingsProperty(account.concat(".password"), password);
			}
		}
		return new String[]{user, password};
	}
	
    /**
     * This method provides access to the applications help set for
     * the JavaHelp system.
     * 
     * @return Returns the helpset.
     */
    public static HelpSet getHelpset()
    {
    	/*
    	 * Loading HelpSet if not already done
    	 */
    	if(helpset == null)
    	{	
			/* 
			 * Loading the HelpSet if a HelpSet file is present in
			 * help/HelpSet.hs. Errors are ignored.
			 */
			try
			{
				// TODO: HelpSet could not be loaded without exception
			    //helpset = new javax.help.HelpSet(null, javax.help.HelpSet.findHelpSet(Application.class.getClassLoader(), "help/HelpSet.hs"));
			}
			catch(Exception exc)
			{
				logger.log(Level.WARNING, "Unable to locate online help definition");
			}
    	}
    	
        return helpset;
    }
    
    /**
     * This method returns a <code>JHelpContentViewer</code> object displaying
     * the given id if available.
     * 
     * @param id
     * @return <code>JHelpContentViewer</code> component
     */
    public static JHelpContentViewer getHelpContentViewer(String id)
    {
        try
        {
	        JHelpContentViewer viewer = new JHelpContentViewer(getHelpset());
			viewer.setCurrentID(id);
			return viewer;
        }
        catch(Exception exc)
        {
        	logger.log(Level.SEVERE, exc.getLocalizedMessage());
            return null;
        }
        
    }
    
	public static String getPropertiesFileName()
	{
		return propertiesFileName;
	}

	public static void setPropertiesFileName(String propertiesFileName)
	{
		Application.propertiesFileName = propertiesFileName;
		logger.info("Used properties file: [" + propertiesFileName + "]");
	}
}
