/*
 * Bootstrapper.java
 * Created on 15.02.2008
 *
 * (c) ALX-Development
 */
 
package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.Frame;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import javax.swing.JFrame;

import de.alx_development.Application.languages.Translator;

/**
 * This class is used as main class for displaying
 * the splash screen and info screen and loading another
 * class using the main method of this class.
 * Use the <code>load</code> method to load the application
 * using a splash screen which displays all log messages
 * with minimum level of INFO.
 * 
 * @author Alexander Thiel
 */

public class Bootstrapper
{	
	/**
	 * Main method for this loader. The given arguments
	 * are used to specify the class to load with this loader.
	 * All other arguments are passed to the class which is loaded by
	 * this class.
	 * 
	 * @param appclass The application class as <code>Class</code> object
	 */
	public static void load(Class<?> appclass)
	{		 
		/*
		 * Extracting the program to load from the arguments array.
		 * Starting the SplashScreen
		 */
		Frame splashScreen = null;
		
		/*
		 * Trying to find the requested class
		 */
		String applicationClassName = appclass.getName();
		Application.setPropertiesFileName(appclass.getSimpleName());
		Application.loadProperties();
		
		if(Boolean.parseBoolean(Application.getSettingsProperty(applicationClassName+".graphicalInterface", "true")))
		{
			String s = MessageFormat.format(Translator.getInstance().getLocalizedString("LOADING"), new Object[]{applicationClassName});
			splashScreen = SplashScreen.splash(s);
		}		
		
		//Launching the application with the given arguments.
		try
		{
			JFrame frame = (JFrame) appclass.getConstructor().newInstance();
			frame.setVisible(true);
		}
		catch (InstantiationException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		//Destroy SplashScreen
		if(splashScreen != null)
		{
			splashScreen.dispose();
		}
	}
}
