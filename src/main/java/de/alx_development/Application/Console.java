/*
 * Console.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */
 
package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import de.alx_development.Application.logging.LoggingPanel;

/**
 * This class is used to create the console output
 * on graphical systems.
 * The standard console output is still active.
 * 
 * @author Alexander Thiel
 */
public class Console extends JDialogFrame
{
	private static final long serialVersionUID = -46985882006126491L;
    
	/**
	 * This constructor which instantiates the console
	 * panel.
	 */
    public Console()
    {
    	super();
    	setTitle("LogConsole");
		
    	LoggingPanel panel = new LoggingPanel();
    	
    	setLayout(new BorderLayout());
    	getContentPane().add(panel, BorderLayout.CENTER);
    	
		/*
		 * Adding additional component listener to be informed about
		 * window state changes which should be stored
		 * in the properties.
		 */
		addComponentListener(new ComponentListener()
				{

					public void componentResized(ComponentEvent event) { }
					public void componentMoved(ComponentEvent event) { }
					
					public void componentShown(ComponentEvent event)
					{
						Application.setSettingsProperty(event.getSource().getClass(), "visible", "true");
					}
					
					public void componentHidden(ComponentEvent event)
					{
						Application.setSettingsProperty(event.getSource().getClass(), "visible", "false");
					}
				});
		
		setVisible(Boolean.parseBoolean(Application.getSettingsProperty(this.getClass(), "visible", "false")));
    }
}
