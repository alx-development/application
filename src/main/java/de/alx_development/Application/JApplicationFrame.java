/*
 * JApplicationFrame.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */
 
package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;

import javax.help.HelpSet;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import de.alx_development.Application.images.ImageLoader;
import de.alx_development.Application.languages.Translator;

/**
 * This Frame extends the standard JFrame and implements the
 * basic functions of each program frame. A menu bar with help
 * is generated, if a help set is found in <code>/help/HelpSet.hs</code>.
 * 
 * @author alex
 */
public class JApplicationFrame extends JFrame implements RootPaneContainer, ContainerListener
{
	private static final long serialVersionUID = 1L;
	
	private Container contentPane;
	private JDialog console;
	private String version = new String();
	private ImageLoader imageloader;
	
	/**
	 * Constructor without any parameters.
	 * 
	 * @throws java.awt.HeadlessException
	 * @see #JApplicationFrame(String)
	 * @see javax.swing.JFrame
	 */
	public JApplicationFrame() throws HeadlessException
	{
		this(null);
	}
	
	/**
	 * Constructor with parameter <code>title</code>.
	 * 
	 * @param title
	 * @throws java.awt.HeadlessException
	 * @see #JApplicationFrame()
	 * @see javax.swing.JFrame
	 */
	public JApplicationFrame(String title) throws HeadlessException
	{
		super();
		if(title == null) title = this.getClass().getName();
		setTitle(title);
	}

	/**
	 * Private method to perform basic component tasks for the
	 * menus and the console output.
	 * 
	 * @see javax.swing.JFrame#frameInit()
	 */
	protected void frameInit()
	{
		super.frameInit();
		
	    contentPane = new Container();
	    imageloader = ImageLoader.getInstance();
	    
		/*
		 * Preparing the content pane as container with
		 * a standard BorderLayout as layout manager.
		 */
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		
		// Loading look'n feel definitions
		Dimension size = new Dimension(
				Integer.valueOf(Application.getSettingsProperty(getClass(), "vsize", "800")).intValue(),
				Integer.valueOf(Application.getSettingsProperty(getClass(), "hsize", "600")).intValue()
			);
		setSize(size);
		
		Point position = new Point(
				Integer.valueOf(Application.getSettingsProperty(getClass(), "vpos", "50")).intValue(),
				Integer.valueOf(Application.getSettingsProperty(getClass(), "hpos", "50")).intValue()
			);
		setLocation(position);
		
		setLookAndFeel(Application.getSettingsProperty(getClass(), "UIlaf", null));
		
		// Setting default closing method
		addWindowListener(new WindowAdapter()
				{
					public void windowClosing(WindowEvent we)
					{
						exit(0);
					}
				});

		/*
		 * Adding additional component listener to be informed about
		 * window size and location changes which should be stored
		 * in the properties.
		 */
		addComponentListener(new ComponentListener()
				{

					public void componentResized(ComponentEvent event)
					{
						/*
						Dimension size = getSize();
						Application.setSettingsProperty(event.getSource().getClass(), "vsize", new Integer(size.width).toString());
						Application.setSettingsProperty(event.getSource().getClass(), "hsize", new Integer(size.height).toString());
						*/
					}

					public void componentMoved(ComponentEvent event)
					{
						/*
						Point position = getLocationOnScreen();
						Application.setSettingsProperty(event.getSource().getClass(), "vpos", new Integer(position.x).toString());
						Application.setSettingsProperty(event.getSource().getClass(), "hpos", new Integer(position.y).toString());
						*/
					}
					
					/*
					 * These events are ignored, cause the application window
					 * should be displayed every time.
					 */
					public void componentShown(ComponentEvent event) { }
					public void componentHidden(ComponentEvent event) { }
				});
		
		/*
		 * Initializing the menu and assigning the localized
		 * languages.
		 */
		JMenu applicationMenu = new JMenu(Translator.getInstance().getLocalizedString("APPLICATION"));
		applicationMenu.setName("APPLICATION");
		
		JMenu helpMenu = new JMenu(Translator.getInstance().getLocalizedString("HELP"));
		helpMenu.setName("HELP");
		
		JMenuItem exitMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("EXIT"), imageloader.getImageIcon("exit"));
		JMenuItem infoMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("INFO"), imageloader.getImageIcon("info"));
		JMenuItem helpMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("HELP"), imageloader.getImageIcon("help"));
		JMenuItem consoleMenuItem = new JMenuItem(Translator.getInstance().getLocalizedString("SHOW_HIDE_CONSOLE"), imageloader.getImageIcon("openterm"));
		
		/*
		 * Generating the console panel which handles the
		 * logging output.
		 */
		try
		{
		    console = new Console();
			
			consoleMenuItem.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							console.setVisible(!console.isVisible());
						}
					});
		}
		catch (Exception e)
		{
			Application.logger.log(Level.WARNING, "Unable to initialize console output.");
		}
		
		/*
		 * Building a menu to provide look and feel
		 * switching depending on which look and feels
		 * are currently supported by the installed JVM
		 */
		JMenu lafMenu = new JMenu(Translator.getInstance().getLocalizedString("LOOKNFEEL"));
		lafMenu.setIcon(imageloader.getImageIcon("laf"));
		
		ActionListener lafActionListener = new ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					JApplicationFrame.this.setLookAndFeel(event.getActionCommand());
				}
			};
			
		UIManager.LookAndFeelInfo lafs[] = UIManager.getInstalledLookAndFeels();
		for(int i=0; i < lafs.length; i++)
		{
			ImageIcon lafImageIcon;
			String lafName = lafs[i].getClassName().substring(lafs[i].getClassName().lastIndexOf(".") + 1).replaceAll("LookAndFeel", new String());
			
			/*
			 * Try to load an ImageIcon for the LookNFeel.
			 * If no icon can be found, a standard icon is
			 * used.
			 */
			lafImageIcon = imageloader.getImageIcon(lafName);
			if(lafImageIcon == null)
				lafImageIcon = imageloader.getImageIcon("defaultLAF");
						
			JMenuItem lafMenuItem = new JMenuItem(lafName, lafImageIcon);
			lafMenuItem.setActionCommand(lafs[i].getClassName());
			lafMenuItem.addActionListener(lafActionListener);
			lafMenu.add(lafMenuItem);
		}
		
		/*
		 * Adding action event handling to the integrated menubar
		 * buttons
		 */	
		exitMenuItem.addActionListener(
			new ActionListener()
			{
		   		public void actionPerformed(ActionEvent e)
		   		{
			  		exit(0);
		   		}
			});

		infoMenuItem.addActionListener(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					SplashScreen.splash(getTitle().concat(" ").concat(version));
				}
			});
		
		/*
		 * Building the menu structure
		 */
		applicationMenu.add(lafMenu);
		applicationMenu.add(consoleMenuItem);
		applicationMenu.addSeparator();
		applicationMenu.add(exitMenuItem);
		helpMenu.add(infoMenuItem);
		
		JMenuBar menubar = new JMenuBar();
		menubar.add(applicationMenu);
		menubar.add(helpMenu);
		setJMenuBar(menubar);
		
		/* 
		 * Loading the helpset if a helpset file is present in
		 * help/HelpSet.hs. Errors are ignored.
		 */
		try
		{
			HelpSet hs = Application.getHelpset();
			if(hs != null)
			{
				helpMenuItem.addActionListener(new javax.help.CSH.DisplayHelpFromSource(hs.createHelpBroker()));
				helpMenu.add(helpMenuItem);
			}
		}
		catch(Exception exc)
		{
			Application.logger.log(Level.WARNING, "Helpsystem could not be loaded");
		}
		
		/*
		 * Setting the default layout for the GUI
		 */
		UIManager.put("TableHeader.foreground", (new Color(99, 99, 124)));
		UIManager.put("TableHeader.font", (new JButton()).getFont());
		
		super.getContentPane().add(getStatusBar(), BorderLayout.SOUTH);
	}
	
	/**
	 * This internal method is used to set the look and feel using
	 * the laf managers class name as parameter.
	 * 
	 * @param lafclassname The class name for the look and feel
	 */
	protected void setLookAndFeel(String lafclassname)
	{
		try
		{
			UIManager.setLookAndFeel(lafclassname);
			SwingUtilities.updateComponentTreeUI(JApplicationFrame.this);
			Application.setSettingsProperty(JApplicationFrame.this.getClass(), "UIlaf", lafclassname);
			Application.logger.log(Level.INFO, "Look and Feel changed to " + lafclassname);
		}
		catch (Exception exc)
		{
			Application.setSettingsProperty(JApplicationFrame.this.getClass(), "UIlaf", null);
			Application.logger.log(Level.CONFIG, "Unable to load Look'n Feel, Using default. "+exc.getMessage());
		}
	}
	
	/**
	 * This function may be used to modify the version string
	 * displayed in the information panel.
	 * 
	 * @param version
	 * 
	 * @see #getVersion
	 */
	public void setVersion(String version)
	{
		this.version=version;
	}
	
	/**
	 * This method returns the version string as defined
	 * using <code>setVersion(String)</code>.
	 * 
	 * @return The vversion as <code>String</code>
	 * 
	 * @see #setVersion
	 */
	public String getVersion()
	{
		return version;
	}
	
	/**
	 * This method gives access to the text of the info row on the bottom
	 * of the application window.
	 * 
	 * @param info String representation of the Infotext.
	 * @see #getInfotext
	 * @see #setInfoicon
	 */
	public void setInfotext(String info)
	{
		if(info == null){ info = " "; }
		getStatusBar().setCenterText(info);
	}
	
	/**
	 * This method gives access to the text in the info row
	 * on the bottom of the application window.
	 * 
	 * @return The text as <code>String</code>
	 * @see #setInfotext
	 * @see #setInfoicon
	 */
	public String getInfotext()
	{
		return getStatusBar().getCenterText();
	}
	
	/**
	 * This method should be used to set an icon in front
	 * of the infotext in the info row.
	 * 
	 * @param icon
	 * @see #getInfotext
	 * @see #setInfotext
	 */
	public void setInfoicon(ImageIcon icon)
	{
		getStatusBar().setIcon(icon);
	}
	
	/**
	 * Returns the <code>contentPane</code> object for this frame.
	 * 
	 * @return the <code>contentPane</code> property
	 * 
	 * @see #setContentPane
	 */
	public Container getContentPane()
	{ 
		return contentPane;
	}
	
	/**
	 * You may set your own ContentPane using this
	 * method. This functionality is added to be able to use the
	 * <code>JExtendedDesktopPane</code> as ContentPane.
	 * 
	 * @param newPane A <code>Container</code> used as ContentPane
	 * 
	 * @see #getContentPane
	 */
	public void setContentPane(Container newPane)
	{
		/*
		 * Removing the old contentPane from the frame
		 */
		contentPane.removeContainerListener(this);
		super.getContentPane().remove(contentPane);
		
		contentPane=newPane;
		
		/*
		 * Setting the parameters for the new content pane.
		 */
		contentPane.getToolkit().setDynamicLayout(true);
		contentPane.addContainerListener(this);
		super.getContentPane().add(contentPane, BorderLayout.CENTER);
		
		contentPane.repaint();
		super.getContentPane().repaint();
	}
	
	/**
	 * Use this method to add a  additional menu item to the
	 * menubar. The menu item is added as last element before
	 * the help menu.
	 * 
	 * @param menuItem
	 */
	public void addMenu(JMenu menuItem)
	{
		/*
		 * The last menu item is the help menu item.
		 * If not, the new one is inserted without reorganizing
		 * the menu.
		 */
	    JMenuBar menubar = getJMenuBar();
		JMenu helpMenu = menubar.getMenu(menubar.getMenuCount()-1);
		if(helpMenu.getName().equals("HELP"))
		{
			menubar.remove(helpMenu);
			menubar.add(menuItem);
			menubar.add(helpMenu);
		}
		else
		{
			menubar.add(menuItem);
		}
	}
	
	/**
	 * This method is implemnted by the ContainerListener and used to redraw
	 * the container content if this has been changed.
	 * 
	 * @param e ContainerEvent
	 */
	public void componentAdded(ContainerEvent e)
	{
		((Container)e.getSource()).validate();
	}

	/**
	 * This method is implemnted by the ContainerListener and used to redraw
	 * the container content if this has been changed.
	 * 
	 * @param e ContainerEvent
	 */
	public void componentRemoved(ContainerEvent e)
	{
		((Container)e.getSource()).validate();
	}
	
	/**
	 * This method may be used to change the application icon shown in
	 * the taskbar as well as the title bar of the application. As
	 * parameter an <code>ImageIcon</code> object is expected.
	 * 
	 * @param icon
	 */
	public void setApplicationIcon(ImageIcon icon)
	{
        Image image = icon.getImage();
        while(!getToolkit().prepareImage(image, -1, -1, this))
        {
           try
           {
             Thread.sleep( 100 );
           }
           catch(Exception e) {}
         }
         setIconImage( image );
	}
	
	/**
	 * This function returns a reference of the
	 * statusbar used in the application window
	 * @return A reference to the statusbar
	 */
	public JApplicationStatusbar getStatusBar()
	{
		return JApplicationStatusbar.getInstance();
	}
	
	/**
	 * Invoke this method to perform a clean shutdown of the
	 * application. Using this method all custom settings made
	 * during the application run are stored in the user properties.
	 * 
	 * @param state The applications exit state
	 */
	public void exit(int state)
	{
		Application.writeProperties();
		System.exit(state);
	}
}
