package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

public class JApplicationStatusbar extends JPanel
{
	private static final long serialVersionUID = -5425825050177967457L;

	private static JApplicationStatusbar instance = null;
	
	private JLabel icon;
	private JLabel centerText;
	private JLabel eastText;
	
	/**
	 * Implementation of a singleton pattern to avoid multiple instances
	 * of <code>JConfigurationDialog</code> at one time.
	 * 
	 * @return The system wide configuration dialog
	 */
	public static JApplicationStatusbar getInstance()
	{
		if(instance == null) instance = new JApplicationStatusbar();
		return instance;
	}
	
	/**
	 * Default constructor which is private to implement the statusbar
	 * using the singleton pattern
	 */
	private JApplicationStatusbar()
	{
		super();
		setLayout(new BorderLayout());
		
		icon = new JLabel(" ");
		eastText = new JLabel(" ");
		centerText = new JLabel(" ");
		
		eastText.setBorder(BorderFactory.createLoweredBevelBorder());
		centerText.setBorder(BorderFactory.createLoweredBevelBorder());
		
		eastText.setPreferredSize(new Dimension(150, 16));
		eastText.setHorizontalAlignment(JLabel.CENTER);
		
		icon.setPreferredSize(new Dimension(32, 16));
		icon.setHorizontalAlignment(JLabel.CENTER);
		icon.setIcon(new ImageIcon(JApplicationStatusbar.class.getResource("images/app.png")));
		
		add(icon, BorderLayout.WEST);
		add(centerText, BorderLayout.CENTER);
		add(eastText, BorderLayout.EAST);
		
		repaint();
	}
	
	/**
	 * Use this method to set the text in the Project name field
	 * inside the status bar
	 * 
	 * @param text
	 */
	public void setEastText(String text)
	{
		eastText.setText(text);
		
	}
	
	/**
	 * Use this method to set a text in the statusbar
	 * 
	 * @param text
	 */
	public void setCenterText(String text)
	{
		centerText.setText(text);
	}

	/**
	 * @return the centerText
	 */
	public String getCenterText()
	{
		return centerText.getText();
	}

	/**
	 * @return the eastText
	 */
	public String getEastText()
	{
		return eastText.getText();
	}
	
	/**
	 * This method should be used to set an icon in front
	 * of the infotext in the status bar.
	 * 
	 * @param icon
	 */
	public void setIcon(ImageIcon icon)
	{
		this.icon.setIcon(icon);
	}
}
