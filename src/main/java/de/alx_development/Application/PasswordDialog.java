/*
 * PasswordDialog.java
 * Created on 25.11.2003
 *
 * (c) ALX-Development
 */

package de.alx_development.Application;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import javax.swing.*;

import de.alx_development.Application.languages.Translator;

import java.awt.*;
import java.awt.event.*;
import java.util.Locale;

/**
 * A modal dialog that asks the user for a user name and password.
 * It is used in the Application is the <code>getAccount</code>
 * function is called.
 * But you can use this without the Application as factory object.
 *
 * @author Alexander Thiel
 */
public class PasswordDialog extends JDialog
{
	private static final long serialVersionUID = 1L;
	
	private JTextField name;
	private JPasswordField pass;
	private JButton okButton;
	private JButton cancelButton;
	private JLabel nameLabel;
	private JLabel passLabel;

	/**
	 * Set the name that appears as the default
	 * An empty string will be used if this in not specified
	 * before the dialog is displayed.
	 * 
	 * @param name default name to be displayed.
	 */
	public void setName(String name)
	{
		this.name.setText(name);
	}

	/**
	 * Set the password that appears as the default
	 * An empty string will be used if this in not specified
	 * before the dialog is displayed.
	 * 
	 * @param pass default password to be displayed.
	 */
	public void setPass(String pass)
	{
		this.pass.setText(pass);
	}

	/**
	 * Set the label on the OK button.
	 * The default is a localized string.
	 * 
	 * @param ok label for the ok button.
	 */
	public void setOKText(String ok)
	{
		okButton.setText(ok);
		pack();
	}

	/**
	 * Set the label on the cancel button.
	 * The default is a localized string.
	 * 
	 * @param cancel label for the cancel button.
	 */
	public void setCancelText(String cancel)
	{
		cancelButton.setText(cancel);
		pack();
	}

	/**
	 * Set the label for the field in which the name is entered.
	 * The default is a localized string.
	 * 
	 * @param name label for the name field.
	 */
	public void setNameLabel(String name)
	{
		nameLabel.setText(name);
		pack();
	}

	/**
	 * Set the label for the field in which the password is entered.
	 * The default is a localized string.
	 * 
	 * @param pass label for the password field.
	 */
	public void setPassLabel(String pass)
	{
		passLabel.setText(pass);
		pack();
	}

	/**
	 * Get the name that was entered into the dialog before
	 * the dialog was closed.
	 * 
	 * @return the name from the name field.
	 */
	public String getName()
	{
		return name.getText();
	}

	/**
	 * Get the password that was entered into the dialog before
	 * the dialog was closed.
	 * 
	 * @return the password from the password field.
	 */
	public String getPassword()
	{
		return new String(pass.getPassword());
	}

	/**
	 * Finds out if user used the OK button or an equivalent action
	 * to close the dialog.
	 * Pressing enter in the password field may be the same as
	 * 'OK' but closing the dialog and pressing the cancel button
	 * are not.
	 * 
	 * @return true if the the user hit OK, false if the user canceled.
	 */
	public boolean okPressed()
	{
		return pressed_OK;
	}

	/**
	 * update this variable when the user makes an action
	 */
	private boolean pressed_OK = false;

	/**
	 * Create this dialog with the given parent and title.
	 * 
	 * @param parent window from which this dialog is launched
	 * @param title the title for the dialog box window
	 */
	public PasswordDialog(Frame parent, String title)
	{
		super(parent, title, true);

		setLocale(Locale.getDefault());

		if(title == null)
		{
			setTitle(Translator.getInstance().getLocalizedString("PWDialog.title"));
		}
		if(parent != null)
		{
			setLocationRelativeTo(parent);
		}
	}

	/**
	 * Create this dialog with the given parent and the default title.
	 * 
	 * @param parent window from which this dialog is launched
	 */
	public PasswordDialog(Frame parent)
	{
		this(parent, null);
	}

	/**
	 * Create this dialog with the default title.
	 */
	public PasswordDialog()
	{
		this(null, null);
	}

	/**
	 * Called by constructors to initialize the dialog.
	 */
	protected void dialogInit()
	{
		name = new JTextField("", 20);
		pass = new JPasswordField("", 20);
		
		okButton = new JButton(Translator.getInstance().getLocalizedString("PWDialog.ok"));
		cancelButton = new JButton(Translator.getInstance().getLocalizedString("PWDialog.cancel"));
		nameLabel = new JLabel(Translator.getInstance().getLocalizedString("PWDialog.NameLabel") + " ");
		passLabel = new JLabel(Translator.getInstance().getLocalizedString("PWDialog.PassLabel") + " ");

		super.dialogInit();

		KeyListener keyListener = (new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE ||  (e.getSource() == cancelButton && e.getKeyCode() == KeyEvent.VK_ENTER))
				{
					pressed_OK = false;
					PasswordDialog.this.setVisible(false);
				}
				if (e.getSource() == okButton && e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					pressed_OK = true;
					PasswordDialog.this.setVisible(false);
				}
			}
		});
		addKeyListener(keyListener);

		ActionListener actionListener = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Object source = e.getSource();
				if (source == name)
				{
					// the user pressed enter in the name field.
					name.transferFocus();
				}
				else
				{
					// other actions close the dialog.
					pressed_OK = (source == pass || source == okButton);
					PasswordDialog.this.setVisible(false);
				}
			}
		};

		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.insets.top = 5;
		c.insets.bottom = 5;
		JPanel pane = new JPanel(gridbag);
		pane.setBorder(BorderFactory.createEmptyBorder(10, 20, 5, 20));

		c.anchor = GridBagConstraints.EAST;
		gridbag.setConstraints(nameLabel, c);
		pane.add(nameLabel);

		gridbag.setConstraints(name, c);
		name.addActionListener(actionListener);
		name.addKeyListener(keyListener);
		pane.add(name);

		c.gridy = 1;
		gridbag.setConstraints(passLabel, c);
		pane.add(passLabel);

		gridbag.setConstraints(pass, c);
		pass.addActionListener(actionListener);
		pass.addKeyListener(keyListener);
		pane.add(pass);

		c.gridy = 2;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		JPanel panel = new JPanel();
		okButton.addActionListener(actionListener);
		okButton.addKeyListener(keyListener);
		panel.add(okButton);
		cancelButton.addActionListener(actionListener);
		cancelButton.addKeyListener(keyListener);
		panel.add(cancelButton);
		gridbag.setConstraints(panel, c);
		pane.add(panel);

		getContentPane().add(pane);

		pack();
		
		/*
		 * Configuring the position for the password dialog.
		 * Default is centering the dialog at the mainscreen.
		 */
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenDim.width - getWidth())/2 , (screenDim.height - getHeight())/2 );
	}

	/**
	 * Shows the dialog and returns true if the user pressed ok.
	 * 
	 * @return true if the the user hit OK, false if the user canceled.
	 */
	public boolean showDialog()
	{
		setVisible(true);
		return okPressed();
	}
}
