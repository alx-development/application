package de.alx_development.Application.images;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.Image;
import java.net.URL;
import java.util.HashMap;

import javax.swing.ImageIcon;

/**
 * This class is defined to load the intenral images and image icons.
 * It ensures, icons are only loaded once and the instances are shared.
 * 
 * @author alex
 */
public class ImageLoader
{
	/**
	 * Internal reference to the instance
	 */
	private static ImageLoader instance;
	
	/**
	 * This map contains the loaded image icons to ensure these are
	 * only loaded once.
	 */
	private HashMap<String, ImageIcon> imagecache = new HashMap<String, ImageIcon>();
	
	/**
	 * Cause the <code>ImageLoader</code> is implemented as singleton
	 * you have to use this function to get a reference to the
	 * instance of the loader.
	 * 
	 * @return A reference to the <code>ImageLoader</code> instance
	 */
	public static ImageLoader getInstance()
	{
		if(instance == null)
			instance = new ImageLoader();
		
		return instance;
	}
	
	/**
	 * Default constructor, which is provate to implement the
	 * singleton pattern. Use the <code>getInstance</code> function
	 * to retrieve an instance reference.
	 * 
	 * @see #getInstance()
	 */
	private ImageLoader()
	{
		super();
	}
	
	/**
	 * This function returns an <code>ImageIcon</code> object defined by the
	 * given name.
	 * 
	 * @param name The name for the icon to return
	 * @return The <code>ImageIcon</code>
	 * @see #getImage(String)
	 */
	public ImageIcon getImageIcon(String name)
	{
		/*
		 * Lookup in cache for an already loaded
		 * image icon
		 */
		ImageIcon icon = imagecache.get(name);
		
		/*
		 * If nothing has been returned from the cache,
		 * try to load it.
		 */
		if(icon == null)
			icon = loadImageIcon(name);
		
		return icon;
	}
	
	/**
	 * As alternative to the swing <code>ImageIcon</code> this provides
	 * the AWT <code>Image</code> representation of the icon.
	 * 
	 * @param name The name for the icon to return
	 * @return The <code>Image</code>
	 * @see #getImageIcon(String)
	 */
	public Image getImage(String name)
	{
		return getImageIcon(name).getImage();
	}
	
	/**
	 * Internally used function to load the requested icon from
	 * the resources.
	 * 
	 * @param name The image name
	 * @return The <code>ImageIcon</code> or null if not available
	 */
	private ImageIcon loadImageIcon(String name)
	{
		URL url;
		ImageIcon icon = null;
		
		url = ImageLoader.class.getResource(name+".gif");
		if(url == null)
			url = ImageLoader.class.getResource(name+".jpg");
		if(url == null)
			url = ImageLoader.class.getResource(name+".png");
		
		/*
		 * If the icon could be loaded successfully from the
		 * resources it should be registered to the cache, to be
		 * available for further access.
		 */
		if(url != null)
		{
			icon = new ImageIcon(url);
			imagecache.put(name, icon);
		}
		
		return icon;
	}
}
