package de.alx_development.Application.logging;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.util.logging.Level;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;

/**
 * The <code>LoggingPanel</code> may be used to display logging information
 * in a console style inside an application.
 * 
 * @author Alexander Thiel
 */
public class LoggingPanel extends JPanel
{
	private static final long serialVersionUID = -1231971872605292735L;
	
	// Maximum count of lines in the Textarea
    private final int MAXLINES = 1000;
    private JScrollPane pane = new JScrollPane();
    private JTextArea textarea = new JTextArea();
    
    /**
     * The default constructor instantiates the component and
     * register it to the <code>LoggingPanelHandler</code>.
     */
    public LoggingPanel()
    {
    	setLayout(new BorderLayout());
    	
        textarea.setEditable(false);
        textarea.setBackground(java.awt.Color.black);
        textarea.setFont(new java.awt.Font("Monospaced", 0, 12));
        textarea.setForeground(java.awt.Color.GREEN);

        add(pane, BorderLayout.CENTER);
        pane.getViewport().add(textarea, null);
        
        /*
         * Initializing the handler which connects to the logging API
         * and appends the messages to this panel.
         */
        LoggingPanelHandler.getInstance().addLoggingPanel(this);
        de.alx_development.Application.Application.logger.log(Level.CONFIG, "Logging console has been initialized successfully");
    }
    
    /**
     * This method is triggered from the <code>LoggingPanelHandler</code> if new
     * logging information is available to display.
     * 
     * @param data The String containing the log message
     */
    protected synchronized void addLogInfo(final String data)
    {
    	EventQueue.invokeLater(new Runnable()
    	{
			public void run()
			{
		    	textarea.append(data);
		    	
		    	// check Linecount
		        int len = textarea.getLineCount();
		        if (len > MAXLINES)
		        {
		            try
		            {
		                textarea.getDocument().remove(0, 5);
		            }
		            catch (javax.swing.text.BadLocationException exception) { }
		        }
		
				 // Scroll down the textarea to the bottom
		        Dimension size = textarea.getSize();
		        JViewport port = pane.getViewport();
		        Point point = new Point(0, size.height);
		        port.setViewPosition(point);
			}
		});
    }
}
