package de.alx_development.Application.logging;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.ErrorManager;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggingPanelHandler extends Handler
{
	private static LoggingPanelHandler instance;
	protected ArrayList<LoggingPanel> loggingpanels = new ArrayList<LoggingPanel>();
	
	/**
	 * The <code>LoggingPanelHandler</code> is implemented as singleton. Use this method to get
	 * a reference to the instance.
	 * 
	 * @return Reference to the <code>LoggingPanelHandler</code> instance.
	 */
	public static synchronized LoggingPanelHandler getInstance()
	{
		if (instance == null)
			instance = new LoggingPanelHandler();
		
		return instance;
	}
	
	/**
	 * The private constructor is implicitly invoked by the <code>getInstance</code>
	 * method, if no instance is yet available. This method is only called once.
	 * The <code>getInstance</code> method will always return a reference to the
	 * single instance.
	 * 
	 * @see #getInstance()
	 */
	private LoggingPanelHandler()
	{
		super();
		
		LogManager manager = LogManager.getLogManager();
		String className = getClass().getName();
		
		/*
		 * Configuring the log level for this logger. If not
		 * defined otherwise the standard log level is <code>INFO</code>
		 */
		String level = manager.getProperty(className + ".level");
		setLevel((level == null) ? Level.INFO : Level.parse(level));
		
		String filterName = manager.getProperty(className + ".filter");
		Filter filter = null;
		try
		{
			Class<?> c = Class.forName(filterName);
			filter = (Filter)c.getConstructor().newInstance();
		}
		catch (Exception e)
		{
			if (filterName != null)
				reportError("Unable to load filter: " + filterName, e, ErrorManager.GENERIC_FAILURE);
		}
		setFilter(filter);
		
		/*
		 * Configuring the formatter as defined in the global settings.
		 * If no setting has been defined the <code>SimpleFormattery</code>
		 * is used as default.
		 */
		String formatterName = manager.getProperty(className + ".formatter");
		Formatter formatter;
		try
		{
			Class<?> c = Class.forName(formatterName);
			formatter = (Formatter)c.getConstructor().newInstance();
		}
		catch (Exception e)
		{
			formatter = new SimpleFormatter();
		}
		setFormatter(formatter);
		
		Logger.getLogger("").addHandler(this);
	}

	/**
	 * Close the Handler and free all associated resources.
	 * The close method will perform a flush and then close the Handler.
	 * After close has been called this Handler should no longer be used.
	 * Method calls may either be silently ignored or may throw runtime exceptions. 
	 */
	@Override
	public void close() throws SecurityException
	{
		flush();
		loggingpanels.clear();
		instance = null;
	}

	/**
	 * Flush any buffered output.
	 */
	@Override
	public void flush()
	{
		// TODO Auto-generated method stub
	}

	/**
	 * Publish a <code>LogRecord</code>.
	 * The logging request was made initially to a <code>Logger</code> object,
	 * which initialized the <code>LogRecord</code> and forwarded it here.
	 * The Handler is responsible for formatting the message,
	 * when and if necessary. The formatting should include localization.
	 * 
	 * @param record Description of the log event
	 */
	@Override
	public void publish(LogRecord record)
	{
		String message = null;
		if (isLoggable(record))
		{
			try
			{
				message = getFormatter().format(record);
			}
			catch (Exception e)
			{
				reportError(null, e, ErrorManager.FORMAT_FAILURE);
				return;
			}
			try
			{
				Iterator<LoggingPanel> itr = loggingpanels.iterator();
				while(itr.hasNext())
				{
					LoggingPanel panel = (LoggingPanel) itr.next();
					panel.addLogInfo(message);
				}
			}
			catch (Exception e)
			{
				reportError(null, e, ErrorManager.WRITE_FAILURE);
			}
		}
	}

	/**
	 * Register a new <code>LoggingPanel</code> to this <code>Handler</code> which should
	 * be used to display the log entries.
	 * 
	 * @param panel The panel (<code>LoggingPanel</code>) to use
	 * @see #removeLoggingPanel(LoggingPanel)
	 */
	public synchronized void addLoggingPanel(LoggingPanel panel)
	{
		loggingpanels.add(panel);
	}
	
	/**
	 * This method removes a previously registered <code>LoggingPanel</code> from this
	 * handler.
	 * 
	 * @param panel The <code>LoggingPanel</code> which should be removed
	 * @see #addLoggingPanel(LoggingPanel)
	 */
	public synchronized void removeLoggingPanel(LoggingPanel panel)
	{
		loggingpanels.remove(panel);
	}
}
