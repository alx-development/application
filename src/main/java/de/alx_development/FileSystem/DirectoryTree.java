/*
 * DirectoryTree.java
 *
 * Created on 5. April 2003
 * (c) ALX-Development
 */

package de.alx_development.FileSystem;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.awt.dnd.*;
import java.awt.datatransfer.*;

import javax.swing.*;
import javax.swing.tree.*;

import java.io.*;

/**
 * This class extends the standard JTree and uses the DirectoryTreeModel to
 * display directory trees.
 * 
 * @author  Alexander Thiel
 */
public class DirectoryTree extends JTree implements DragGestureListener, DragSourceListener
{
	private static final long serialVersionUID = 1L;
	
	private DirectoryTreeModel model = new DirectoryTreeModel();
	
	/**
	 * Constructor without a filename filter. All files in the directory
	 * structure are displayed if the DirectoryTree is instanced using this
	 * constructor. The default directory (home) is used as root.
	 */
	public DirectoryTree()
	{
		model.setRoot(new File(System.getProperty("user.home")));
		setModel(model);
		setCellRenderer(new DirectoryCellRenderer());
		getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		// Preparing DirectoryTree for dragging
		DragSource dragSource = DragSource.getDefaultDragSource();
		dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
	}
	
	/**
	 * This method should be used to change the root directory
	 * for the tree.
	 * 
	 * @param root
	 */
	public void setRoot(File root)
	{
		model.setRoot(root);
	}
	
	/**
	 * This method provides access to the current root directory
	 * of the used DirectoryTreeModel.
	 * 
	 * @return Root directory
	 */
	public File getRoot()
	{
		return (File)model.getRoot();
	}
	
	/**
	 * This method may be used to asign a FilenameFilter to
	 * the DirectoryTree.
	 * 
	 * @param filter
	 */
	public void setFilenameFilter(FilenameFilter filter)
	{
		model.setFilenameFilter(filter);
	}
	
	/**
	 * This method returns the actual selected file/directory as
	 * File object.
	 * 
	 * @return The File object representing the selected file/directory
	 */
	public File getSelectedFile()
	{
		return (File) this.getSelectionPath().getLastPathComponent();
	}
	
	/**
	 * Mit Hilfe der dragGestureRecognized-Methode gibt der dragGestureRecognizer
	 * an den DragGestureListener weiter, wann und was f&uuml;r ein Drag gestartet wurde.
	 * @param e DragGestureEvent
	 */
	public void dragGestureRecognized(DragGestureEvent e)
	{
		e.startDrag(DragSource.DefaultCopyDrop, new StringSelection(this.getSelectedFile().toString()), this);
	}
	
	/**
	 * Am Ende des Drags wird dragDropEnd aufgerufen - das dazugeh&ouml;rige Event
	 * kann man dann mit getDropSuccess() nach dem Erfolg des Drags fragen und zum
	 * Beispiel bei Move das gedraggte Objekt l&ouml;schen
	 * Da der Tree nicht berabeitungsf&auml;hig ist ist wird diese Methode nicht verwendet
	 * @param dsde DragSourceDropEvent
	 */
	public void dragDropEnd(DragSourceDropEvent dsde){}
	
	/**
	 * Ein DragSourceListener hat die Methode dragEnter, mit der man zum Beispiel das
	 * Aussehen des Cursors ver&auml;ndern kann. Unter nicht-Win32-Systemen kann
	 * man auch eigene Bilder f&uuml;r den Cursor verwenden
	 * @param dsde DragSourceDragEvent
	 */
	public void dragEnter(DragSourceDragEvent dsde){}
	
	/**
	 * Ein DragSourceListener hat die Methode dragExit, mit der man zum Beispiel das
	 * Aussehen des Cursors ver&auml;ndern kann. Unter nicht-Win32-Systemen kann
	 * man auch eigene Bilder f&uuml;r den Cursor verwenden
	 * @param dse DragSourceEvent
	 */
	public void dragExit(DragSourceEvent dse){}
	
	/**
	 * Ein DragSourceListener hat die Methode dragOver, mit der man zum Beispiel das
	 * Aussehen des Cursors ver&auml;ndern kann. Unter nicht-Win32-Systemen kann
	 * man auch eigene Bilder f&uuml;r den Cursor verwenden
	 * @param dsde DragSourceDragEvent
	 */
	public void dragOver(DragSourceDragEvent dsde){}
	
	/**
	 * Die Methode dropActionChanged wird aufgerufen, wenn sich die Drop-Aktion
	 * ver&auml;ndert hat - der User hat zum Beispiel mit Control und Maus angefangen zu
	 * draggen und dann Control losgelassen. So kann man &uuml;berpr&uuml;fen, ob
	 * das Draggen dann noch erlaubt ist.
	 * Diese Methode wird hier aber nicht verwendet
	 * @param dsde DragSourceDragEvent
	 */
	public void dropActionChanged(DragSourceDragEvent dsde){}
}
