package de.alx_development.FileSystem.Observer;

/*-
 * #%L
 * Application base feature library
 * %%
 * Copyright (C) 2013 - 2019 ALX-Development
 * %%
 * This file is part of the de.alx-development.application library.
 * 
 * The application library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Diese Datei ist Teil der de.alx-development.application Bibliothek.
 * 
 * Die Application-Bibliothek ist Freie Software: Sie können sie unter den
 * Bedingungen der GNU Lesser General Public License, wie von der
 * Free Software Foundation, Version 3 der Lizenz oder (nach Ihrer Wahl)
 * jeder neueren veröffentlichten Version, weiter verteilen und/oder modifizieren.
 * 
 * Diese Bibliothek wird in der Hoffnung, dass sie nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU Lesser General Public License für weitere Details.
 * 
 * Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 * 
 * Copyright (c) 2019 ALX-Development
 * https://www.alx-development.de/
 * #L%
 */

import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpObserver extends Observer
{
	private Pattern filePattern = Pattern.compile(".*");
	private Pattern pathPattern = Pattern.compile(".*");
	
	/**
	 * Constructor which expects a regular expression as string. The expression
	 * is used to filter filenames on which changes the observer should react.
	 * 
	 * @param path
	 * @param watchSubtree
	 * @param pathregexp
	 * @param fileregexp
	 * @throws FileNotFoundException
	 */
	public RegExpObserver(String path, boolean watchSubtree, String pathregexp, String fileregexp) throws FileNotFoundException
	{
		this(path, watchSubtree, Pattern.compile(pathregexp), Pattern.compile(fileregexp));
	}
	
	/**
	 * Constructor which expects a regular expression as pattern. The expression
	 * is used to filter filenames on which changes the observer should react.
	 * 
	 * @param path
	 * @param watchSubtree
	 * @param pathregexp
	 * @param fileregexp
	 * @throws FileNotFoundException
	 */
	public RegExpObserver(String path, boolean watchSubtree, Pattern pathregexp, Pattern fileregexp) throws FileNotFoundException
	{
		this(path, watchSubtree);
		setPathPattern(pathregexp);
		setFilePattern(fileregexp);
	}
	
	/**
	 * Default constructor, which implements regular patterns allowing all
	 * names in either path and file (.*) implicitly.
	 * 
	 * @param path
	 * @param watchSubtree
	 * @throws FileNotFoundException
	 */
	public RegExpObserver(String path, boolean watchSubtree) throws FileNotFoundException
	{
		super(path, watchSubtree);
	}
	
	/**
	 * This function implements the regular expression filter which alows only
	 * to react on file with a matchin filename.
	 * 
	 * @param path The path to the file
	 * @param file the filename
	 * @return true, if filename matches the pattern
	 */
	@Override
	public boolean isValid(String path, String file)
	{
		Matcher filematcher = getFilePattern().matcher(file);
		Matcher pathmatcher = getPathPattern().matcher(path);
		return (filematcher.matches() && pathmatcher.matches());
	}

	/**
	 * This function returns the actual regular expression pattern which
	 * is used to filter the filename.
	 * 
	 * @return the filePattern
	 */
	public Pattern getFilePattern()
	{
		return filePattern;
	}

	/**
	 * Use this method to set a regular expression pattern which should
	 * be used to filter the filename.
	 * 
	 * @param filePattern the filePattern to set
	 */
	public void setFilePattern(Pattern filePattern)
	{
		if(filePattern != null)
			this.filePattern = filePattern;
	}

	/**
	 * This function returns the regular expression pattern which is used to
	 * filter the path name.
	 * 
	 * @return the pathPattern
	 */
	public Pattern getPathPattern()
	{
		return pathPattern;
	}

	/**
	 * Use this method to set a pattern for the path name filter.
	 * 
	 * @param pathPattern the pathPattern to set
	 */
	public void setPathPattern(Pattern pathPattern)
	{
		if(pathPattern != null)
			this.pathPattern = pathPattern;
	}

}
